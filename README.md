# Demo Laravel App - Infrastructure

This project contains very simplistic code that runs containers built
via automated CI pipeline in Laravel App project.

It depends on two containers existing in repository on URL
https://gitlab.com/12fac1019sae/sae-2019-12-factor/container_registry:

* `registry.gitlab.com/12fac1019sae/sae-2019-12-factor:latest`
* `registry.gitlab.com/12fac1019sae/sae-2019-12-factor:latest-nginx`

## Running the application

1. `cp .env.dist .env`
2. Customize contents of `.env` file if necessary
3. `docker-compose up`

Application should be accessible via URL http://localhost:7777